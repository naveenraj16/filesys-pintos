#include "filesys/inode.h"
#include <list.h>
#include <debug.h>
#include <round.h>
#include <string.h>
#include <stdio.h>
#include "filesys/filesys.h"
#include "filesys/free-map.h"
#include "kernel/malloc.h"
#include "kernel/synch.h"

/* Identifies an inode. */
#define INODE_MAGIC 0x494e4f44

#define NUM_DIRECT_BLOCKS 8
#define NUM_INDIRECT_BLOCKS 4
#define NUM_DOUBLE_INDIRECT_BLOCKS 1
#define NUM_INDIRECT_BLOCK_PTRS 128
#define NUM_INODE_BLOCK_PTRS 13
#define DIRECT_INDEX 0
#define INDIRECT_INDEX 8
#define DOUBLE_INDIRECT_INDEX 12

#define MAX_FILE_SIZE (8+512+16384)*512  // 8,654,848 bytes or slightly more than 8MB

#define MIN(a,b)  ((((a)-(b))&0x80000000) >> 31)? (a) : (b)

void inode_deallocate (struct inode *inode);
off_t inode_grow(struct inode *inode, off_t new_length);
size_t allocate_double_indirect_blocks(struct inode *inode, size_t sectors,
                            size_t start_i, size_t start_j);
size_t allocate_indirect_blocks(struct inode *inode, size_t sectors, size_t start_i, size_t start_j);

/* On-disk inode.
   Must be exactly BLOCK_SECTOR_SIZE bytes long. */
struct inode_disk
  {
    off_t length;                       /* File size in bytes. */
    unsigned magic;                     /* Magic number. */

    uint32_t direct_index;
    uint32_t indirect_index;
    uint32_t double_indirect_index;
    block_sector_t ptr[NUM_INODE_BLOCK_PTRS];     /* Pointers to blocks */
    bool isdir;  /* Is this a directory */
    block_sector_t parent; // parent directory inode 


    uint32_t unused[108];                /* Not used. */
  };

struct indirect_block
  {
    block_sector_t ptr[NUM_INDIRECT_BLOCK_PTRS];
  };

/* Returns the number of sectors to allocate for an inode SIZE
   bytes long. */
static inline size_t
bytes_to_sectors (off_t size)
{
  return DIV_ROUND_UP (size, BLOCK_SECTOR_SIZE);
}

/* In-memory inode. */
struct inode 
  {
    struct list_elem elem;              /* Element in inode list. */
    block_sector_t sector;              /* Sector number of disk location. */
    int open_cnt;                       /* Number of openers. */
    bool removed;                       /* True if deleted, false otherwise. */
    int deny_write_cnt;                 /* 0: writes ok, >0: deny writes. */

    off_t length;                       /* File size in bytes. */

    uint32_t direct_index;
    uint32_t indirect_index;
    uint32_t double_indirect_index;
    block_sector_t ptr[NUM_INODE_BLOCK_PTRS];     /* Pointers to blocks */
    bool isdir;  /* Is this a directory */
    block_sector_t parent; // parent directory inode 
    struct indirect_block ib[NUM_INDIRECT_BLOCKS];

    struct lock inode_lock;
  };

/* Returns the block device sector that contains byte offset POS
   within INODE.
   Returns -1 if INODE does not contain data for a byte at offset
   POS. */
static block_sector_t
byte_to_sector (const struct inode *inode, off_t pos) 
{
  ASSERT (inode != NULL);
  if (pos < inode->length){
    if(pos < (NUM_DIRECT_BLOCKS*BLOCK_SECTOR_SIZE)){
      return inode->ptr[pos/BLOCK_SECTOR_SIZE]; 
    }
    else if(pos < ((NUM_DIRECT_BLOCKS+NUM_INDIRECT_BLOCKS*NUM_INDIRECT_BLOCK_PTRS)*BLOCK_SECTOR_SIZE)){
      pos -= NUM_DIRECT_BLOCKS*BLOCK_SECTOR_SIZE;
      uint32_t index;
      index = NUM_DIRECT_BLOCKS + (pos/(NUM_INDIRECT_BLOCK_PTRS*BLOCK_SECTOR_SIZE)); 
      uint32_t indirect_block[NUM_INDIRECT_BLOCK_PTRS];
      block_read(fs_device,inode->ptr[index],&indirect_block); 
      pos = pos%(NUM_INDIRECT_BLOCK_PTRS*BLOCK_SECTOR_SIZE); //offset in the 64K bytes
      return indirect_block[pos/BLOCK_SECTOR_SIZE];
    }
    else{
      pos -= (NUM_DIRECT_BLOCKS + NUM_INDIRECT_BLOCKS*NUM_INDIRECT_BLOCK_PTRS) *BLOCK_SECTOR_SIZE;
      uint32_t index;
      uint32_t indirect_block[NUM_INDIRECT_BLOCK_PTRS];
      block_read(fs_device,inode->ptr[DOUBLE_INDIRECT_INDEX],&indirect_block); 
      index = pos/(NUM_INDIRECT_BLOCK_PTRS*BLOCK_SECTOR_SIZE);
      block_read(fs_device,indirect_block[index],&indirect_block); 
      pos = pos%(NUM_INDIRECT_BLOCK_PTRS*BLOCK_SECTOR_SIZE); //offset in the 64K bytes
      return indirect_block[pos/BLOCK_SECTOR_SIZE];
    }
  }
  else
    return -1;
}

/* List of open inodes, so that opening a single inode twice
   returns the same `struct inode'. */
static struct list open_inodes;

/* Initializes the inode module. */
void
inode_init (void) 
{
  list_init (&open_inodes);
}

bool
inode_create (block_sector_t sector, off_t length)
{
  struct inode_disk *disk_inode = NULL;
  bool success = false;

  ASSERT (length >= 0);

  /* If this assertion fails, the inode structure is not exactly
     one sector in size, and you should fix that. */
  ASSERT (sizeof *disk_inode == BLOCK_SECTOR_SIZE);

  disk_inode = calloc (1, sizeof *disk_inode);
  if (disk_inode != NULL)
    {
      static char zeros[BLOCK_SECTOR_SIZE];
      size_t sectors = bytes_to_sectors (length);
      disk_inode->length = length;
      disk_inode->magic = INODE_MAGIC;
      // Allocate direct blocks
      size_t i = 0;
      disk_inode->direct_index = DIRECT_INDEX;
      while ((sectors > 0) && (i < NUM_DIRECT_BLOCKS)) {
        if (!free_map_allocate (1, &disk_inode->ptr[i]))
          return false;
        block_write(fs_device, disk_inode->ptr[i], zeros);
        disk_inode->direct_index++;
        sectors--;
        i++;
      }

// Allocate indirect blocks
      i = INDIRECT_INDEX;
      disk_inode->indirect_index = INDIRECT_INDEX;
      while ((sectors > 0) && (i < DOUBLE_INDIRECT_INDEX)) {
        size_t j = 0;
        block_sector_t ibp; // ibp = indirect block pointer
        if (!free_map_allocate (1, &ibp))
          return false;
        struct indirect_block ib;
        while ((sectors > 0) && (j < NUM_INDIRECT_BLOCK_PTRS)) {
          if(!free_map_allocate (1, &ib.ptr[j]))
            return false;
          // fill in data sectors with zeros
          block_write(fs_device, ib.ptr[j], zeros);
          sectors--;
          j++;
        }
        // copy pointers to data sectors from ib to ibp
        block_write(fs_device,ibp,&ib);
        disk_inode->indirect_index++;
        disk_inode->ptr[i] = ibp;
        i++;
      }

// Allocate double indirect blocks
      i = 0;
      disk_inode->double_indirect_index = DOUBLE_INDIRECT_INDEX;
      if (!free_map_allocate (1, &disk_inode->ptr[DOUBLE_INDIRECT_INDEX]))
        return false;
      struct indirect_block ib;
      while ((sectors > 0) && (i < NUM_INDIRECT_BLOCK_PTRS)) {
        size_t j = 0;
        if (!free_map_allocate (1, &ib.ptr[i]))
          return false;
        struct indirect_block iib;
        while ((sectors > 0) && (j < NUM_INDIRECT_BLOCK_PTRS)) {
          if (!free_map_allocate (1, &iib.ptr[j]))
            return false;
          block_write(fs_device, iib.ptr[j], zeros);
          sectors--;
          j++;
        }
        block_write(fs_device,ib.ptr[i],&iib);
        i++;
      }
      block_write(fs_device,disk_inode->ptr[DOUBLE_INDIRECT_INDEX],&ib);
      
      // write the disk inode to the disk
      block_write(fs_device,sector,disk_inode);
      success = true;

      // free disk_inode from memory
      free (disk_inode);
    }
  return success;
}


/* Reads an inode from SECTOR
   and returns a `struct inode' that contains it.
   Returns a null pointer if memory allocation fails. */
struct inode *
inode_open (block_sector_t sector)
{
  struct list_elem *e;
  struct inode *inode;

  /* Check whether this inode is already open. */
  for (e = list_begin (&open_inodes); e != list_end (&open_inodes);
       e = list_next (e)) 
    {
      inode = list_entry (e, struct inode, elem);
      if (inode->sector == sector) 
        {
          inode_reopen (inode);
          return inode; 
        }
    }

  /* Allocate memory. */
  inode = malloc (sizeof *inode);
  if (inode == NULL)
    return NULL;

  /* Initialize. */
  list_push_front (&open_inodes, &inode->elem);
  inode->sector = sector;
  inode->open_cnt = 1;
  inode->deny_write_cnt = 0;
  inode->removed = false;
  lock_init(&inode->inode_lock);
  struct inode_disk data; 
  block_read (fs_device, inode->sector, &data);
  inode->length = data.length;
  inode->direct_index = data.direct_index;
  inode->indirect_index = data.indirect_index;
  inode->double_indirect_index = data.double_indirect_index;
  inode->isdir = data.isdir;
  inode->parent = data.parent;
  size_t i = 0;
  for(i = 0; i < NUM_INODE_BLOCK_PTRS; i++){
    inode->ptr[i] = data.ptr[i];
  }
  return inode;
}

/* Reopens and returns INODE. */
struct inode *
inode_reopen (struct inode *inode)
{
  if (inode != NULL)
    inode->open_cnt++;
  return inode;
}

/* Returns INODE's inode number. */
block_sector_t
inode_get_inumber (const struct inode *inode)
{
  return inode->sector;
}

/* Closes INODE and writes it to disk. (Does it?  Check code.)
   If this was the last reference to INODE, frees its memory.
   If INODE was also a removed inode, frees its blocks. */
void
inode_close (struct inode *inode) 
{
  /* Ignore null pointer. */
  if (inode == NULL)
    return;

  /* Release resources if this was the last opener. */
  if (--inode->open_cnt == 0)
    {
      /* Remove from inode list and release lock. */
      list_remove (&inode->elem);

      //inode_remove(inode);
      /* Deallocate blocks if removed. */
      if (inode->removed) 
        {
          free_map_release (inode->sector, 1);
          inode_deallocate(inode);
        }
      /* Should write back the changes made to the file to the disk, but kernel panics on block_write */
      // else{
      //   struct inode_disk data;
      // 	data.length = inode->length;
      // 	data.magic = INODE_MAGIC;
      // 	data.direct_index = inode->direct_index;
      // 	data.indirect_index = inode->indirect_index;
      // 	data.double_indirect_index = inode->double_indirect_index;
      //   data.isdir = inode->isdir;
      //  	data.parent = inode->parent;
      // 	size_t i;
      // 	for(i = 0; i < NUM_INODE_BLOCK_PTRS; i++)
      // 	 data.ptr[i] = inode->ptr[i];
	     // //write the changes back to the disk
	     // block_write(fs_device,inode->sector,&data);
      // }
      

      free (inode); 
    }
}

void inode_deallocate(struct inode *inode) {
  if (inode == NULL)
    return;

  size_t sectors = bytes_to_sectors (inode->length);

  // Deallocate direct blocks
  size_t i = 0;
  while ((sectors > 0) && (i < NUM_DIRECT_BLOCKS)) {
    free_map_release (inode->ptr[i], 1);
    sectors--;
    i++;
  }

  // Deallocate indirect blocks
  i = INDIRECT_INDEX;
  while ((sectors > 0) && (i < DOUBLE_INDIRECT_INDEX)) {
    size_t j = 0;
    struct indirect_block ib;
    block_read(fs_device,inode->ptr[i],&ib);
    while ((sectors > 0) && (j < NUM_INDIRECT_BLOCK_PTRS)) {
      free_map_release (ib.ptr[j], 1);
      sectors--;
      j++;
    }
    //release the indirect block
    free_map_release(inode->ptr[i],1);
      i++;
  } 

  // Dellocate double indirect blocks
  if(sectors == 0)
  	return;
  i = 0;
  struct indirect_block ib;
  block_read(fs_device,inode->ptr[DOUBLE_INDIRECT_INDEX],&ib);
  while ((sectors > 0) && (i < NUM_INDIRECT_BLOCK_PTRS)) {
    size_t j = 0;
  	//read the inner block
    struct indirect_block iib;
    block_read(fs_device,ib.ptr[i],&iib);

    while ((sectors > 0) && (j < NUM_INDIRECT_BLOCK_PTRS)) {
      free_map_release (iib.ptr[j], 1);
      sectors--;
      j++;
    }
      free_map_release (ib.ptr[i], 1);
      i++;
  }
  free_map_release(inode->ptr[DOUBLE_INDIRECT_INDEX],1);

}


/* Marks INODE to be deleted when it is closed by the last caller who
   has it open. */
void
inode_remove (struct inode *inode) 
{
  ASSERT (inode != NULL);
  inode->removed = true;
}

/* Reads SIZE bytes from INODE into BUFFER, starting at position OFFSET.
   Returns the number of bytes actually read, which may be less
   than SIZE if an error occurs or end of file is reached. */
off_t
inode_read_at (struct inode *inode, void *buffer_, off_t size, off_t offset) 
{
  uint8_t *buffer = buffer_;
  off_t bytes_read = 0;
  uint8_t *bounce = NULL;

  while (size > 0) 
    {
      /* Disk sector to read, starting byte offset within sector. */
      block_sector_t sector_idx = byte_to_sector (inode, offset);
      int sector_ofs = offset % BLOCK_SECTOR_SIZE;

      /* Bytes left in inode, bytes left in sector, lesser of the two. */
      off_t inode_left = inode_length (inode) - offset;
      int sector_left = BLOCK_SECTOR_SIZE - sector_ofs;
      int min_left = inode_left < sector_left ? inode_left : sector_left;

      /* Number of bytes to actually copy out of this sector. */
      int chunk_size = size < min_left ? size : min_left;
      if (chunk_size <= 0)
        break;

      if (sector_ofs == 0 && chunk_size == BLOCK_SECTOR_SIZE)
        {
          /* Read full sector directly into caller's buffer. */
          block_read (fs_device, sector_idx, buffer + bytes_read);
        }
      else 
        {
          /* Read sector into bounce buffer, then partially copy
             into caller's buffer. */
          if (bounce == NULL) 
            {
              bounce = malloc (BLOCK_SECTOR_SIZE);
              if (bounce == NULL)
                break;
            }
          block_read (fs_device, sector_idx, bounce);
          memcpy (buffer + bytes_read, bounce + sector_ofs, chunk_size);
        }
      
      /* Advance. */
      size -= chunk_size;
      offset += chunk_size;
      bytes_read += chunk_size;
    }
  free (bounce);

  return bytes_read;
}

/* Writes SIZE bytes from BUFFER into INODE, starting at OFFSET.
   Returns the number of bytes actually written, which may be
   less than SIZE if end of file is reached or an error occurs.
   (Normally a write at end of file would extend the inode, but
   growth is not yet implemented.) */
off_t
inode_write_at (struct inode *inode, const void *buffer_, off_t size,
                off_t offset) 
{
  const uint8_t *buffer = buffer_;
  off_t bytes_written = 0;
  uint8_t *bounce = NULL;

  if (inode->deny_write_cnt)
    return 0;

  if((offset + size) > inode->length){
    lock_acquire(&inode->inode_lock);
    inode->length = inode_grow(inode,offset+size);
    lock_release(&inode->inode_lock);
  }
  while (size > 0) 
    {
      /* Sector to write, starting byte offset within sector. */
      block_sector_t sector_idx = byte_to_sector (inode, offset);
      int sector_ofs = offset % BLOCK_SECTOR_SIZE;

      /* Bytes left in inode, bytes left in sector, lesser of the two. */
      off_t inode_left = inode_length (inode) - offset;
      int sector_left = BLOCK_SECTOR_SIZE - sector_ofs;
      int min_left = inode_left < sector_left ? inode_left : sector_left;

      /* Number of bytes to actually write into this sector. */
      int chunk_size = size < min_left ? size : min_left;
      if (chunk_size <= 0)
        break;

      if (sector_ofs == 0 && chunk_size == BLOCK_SECTOR_SIZE)
        {
          /* Write full sector directly to disk. */
          block_write (fs_device, sector_idx, buffer + bytes_written);
        }
      else 
        {
          /* We need a bounce buffer. */
          if (bounce == NULL) 
            {
              bounce = malloc (BLOCK_SECTOR_SIZE);
              if (bounce == NULL)
                break;
            }

          /* If the sector contains data before or after the chunk
             we're writing, then we need to read in the sector
             first.  Otherwise we start with a sector of all zeros. */
          if (sector_ofs > 0 || chunk_size < sector_left) 
            block_read (fs_device, sector_idx, bounce);
          else
            memset (bounce, 0, BLOCK_SECTOR_SIZE);
          memcpy (bounce + sector_ofs, buffer + bytes_written, chunk_size);
          block_write (fs_device, sector_idx, bounce);
        }

      /* Advance. */
      size -= chunk_size;
      offset += chunk_size;
      bytes_written += chunk_size;
    }
  free (bounce);

  return bytes_written;
}

/* Disables writes to INODE.
   May be called at most once per inode opener. */
void
inode_deny_write (struct inode *inode) 
{
  inode->deny_write_cnt++;
  ASSERT (inode->deny_write_cnt <= inode->open_cnt);
}

/* Re-enables writes to INODE.
   Must be called once by each inode opener who has called
   inode_deny_write() on the inode, before closing the inode. */
void
inode_allow_write (struct inode *inode) 
{
  ASSERT (inode->deny_write_cnt > 0);
  ASSERT (inode->deny_write_cnt <= inode->open_cnt);
  inode->deny_write_cnt--;
}

/* Returns the length, in bytes, of INODE's data. */
off_t
inode_length (const struct inode *inode)
{
  return inode->length;
}

//grow the size of the file
off_t inode_grow(struct inode *inode, off_t new_length){
  static char zeros[BLOCK_SECTOR_SIZE];
  size_t new_sectors = bytes_to_sectors(new_length) - bytes_to_sectors(inode->length);
  if(new_sectors == 0)
    return new_length;
  
  size_t i = bytes_to_sectors(inode->length);
  if(i < INDIRECT_INDEX){
    while ((new_sectors > 0) && (i < INDIRECT_INDEX)) {
        if (!free_map_allocate (1, &inode->ptr[i]))
          return false;
        block_write(fs_device, inode->ptr[i], zeros);
        inode->direct_index++;
        new_sectors--;
        i++;
      }    
    new_sectors = allocate_indirect_blocks(inode,new_sectors,INDIRECT_INDEX,0);
    new_sectors = allocate_double_indirect_blocks(inode,new_sectors,0,0);
  }

  else if(i < (NUM_DIRECT_BLOCKS + NUM_INDIRECT_BLOCKS*NUM_INDIRECT_BLOCK_PTRS)){
    size_t num_bytes = new_length - NUM_DIRECT_BLOCKS*BLOCK_SECTOR_SIZE; 
    size_t indirect_ptr_bytes = NUM_INDIRECT_BLOCK_PTRS*BLOCK_SECTOR_SIZE;
    size_t start_i = INDIRECT_INDEX + (num_bytes/indirect_ptr_bytes);
    size_t start_j = (num_bytes%indirect_ptr_bytes)/BLOCK_SECTOR_SIZE;
    new_sectors = allocate_indirect_blocks(inode,new_sectors,start_i,start_j);
    new_sectors = allocate_double_indirect_blocks(inode,new_sectors,0,0);
  }

  else{
    size_t num_bytes = new_length - (NUM_DIRECT_BLOCKS + NUM_INDIRECT_BLOCKS*NUM_INDIRECT_BLOCK_PTRS)*BLOCK_SECTOR_SIZE; 
    size_t indirect_ptr_bytes = NUM_INDIRECT_BLOCK_PTRS*BLOCK_SECTOR_SIZE;
    size_t start_i = INDIRECT_INDEX + (num_bytes/indirect_ptr_bytes);
    size_t start_j = (num_bytes%indirect_ptr_bytes)/BLOCK_SECTOR_SIZE;
    new_sectors = allocate_double_indirect_blocks(inode,new_sectors,start_i,start_j);
  }

  return new_length;
}


size_t allocate_indirect_blocks(struct inode *inode,size_t sectors,size_t start_i,size_t start_j){
  static char zeros[BLOCK_SECTOR_SIZE];
  if(sectors == 0)
    return sectors;
  if(start_j != 0){
    struct indirect_block ib;
    size_t j = start_j;
    block_read(fs_device,inode->ptr[start_i],&ib);
      while ((sectors > 0) && (j < NUM_INDIRECT_BLOCK_PTRS)) {
        if(!free_map_allocate (1, &ib.ptr[j]))
          return false;
        // fill in data sectors with zeros
        block_write(fs_device, ib.ptr[j], zeros);
        sectors--;
        j++;
      }
  }

  size_t i = 1 + start_i;
  //if it starts a the beginning of indirect blocks
  if((start_j == 0) && (start_i == INDIRECT_INDEX))
    i = start_i;

  while ((sectors > 0) && (i < DOUBLE_INDIRECT_INDEX)) {
        size_t j = 0;
        block_sector_t ibp; // ibp = indirect block pointer
        if (!free_map_allocate (1, &ibp))
          return false;
        struct indirect_block ib;
        while ((sectors > 0) && (j < NUM_INDIRECT_BLOCK_PTRS)) {
          if(!free_map_allocate (1, &ib.ptr[j]))
            return false;
          // fill in data sectors with zeros
          block_write(fs_device, ib.ptr[j], zeros);
          sectors--;
          j++;
        }
        // copy pointers to data sectors from ib to ibp
        block_write(fs_device,ibp,&ib);
        inode->indirect_index++;
        inode->ptr[i] = ibp;
        i++;
      }
  return sectors;
}
size_t allocate_double_indirect_blocks(struct inode *inode,size_t sectors,size_t start_i,size_t start_j){
  static char zeros[BLOCK_SECTOR_SIZE];
  if(sectors == 0)
    return sectors;
    
//if we havent allocated the first indirect block. 
    if ((start_i == 0) && (start_j == 0)) {
    size_t i = 0;
    if (!free_map_allocate (1, &inode->ptr[DOUBLE_INDIRECT_INDEX]))
      return false;
    struct indirect_block ib;
    while ((sectors > 0) && (i < NUM_INDIRECT_BLOCK_PTRS)) {
      size_t j = 0;
      if (!free_map_allocate (1, &ib.ptr[i]))
        return false;
      struct indirect_block iib;
      while ((sectors > 0) && (j < NUM_INDIRECT_BLOCK_PTRS)) {
        if (!free_map_allocate (1, &iib.ptr[j]))
          return false;
        block_write(fs_device, iib.ptr[j], zeros);
        sectors--;
        j++;
      }
      block_write(fs_device,ib.ptr[i],&iib);
      i++;
    }
  block_write(fs_device,inode->ptr[DOUBLE_INDIRECT_INDEX],&ib);
  }

  //first level is created and we need to create the second level indirect blocks
  else if ((start_i != 0) && (start_j == 0)) {
    size_t i = start_i;
    struct indirect_block ib;
    block_read(fs_device, inode->ptr[DOUBLE_INDIRECT_INDEX], &ib);
    if (!free_map_allocate (1, &ib.ptr[i]))
      return false;
    while ((sectors > 0) && (i < NUM_INDIRECT_BLOCK_PTRS)) {
      size_t j = 0;
      if (!free_map_allocate (1, &ib.ptr[i]))
        return false;
      struct indirect_block iib;
      while ((sectors > 0) && (j < NUM_INDIRECT_BLOCK_PTRS)) {
        if (!free_map_allocate (1, &iib.ptr[j]))
          return false;
        block_write(fs_device, iib.ptr[j], zeros);
        sectors--;
        j++;
      }
      block_write(fs_device,ib.ptr[i],&iib);
      i++;
    }
  }
  else {  // start_i != 0 and start_j != 0
    // finish current IIB
    size_t i = start_i;
    size_t j = start_j;
    struct indirect_block ib;
    block_read(fs_device, inode->ptr[DOUBLE_INDIRECT_INDEX], &ib);
    struct indirect_block iib;
    block_read(fs_device, ib.ptr[j], &iib);
    while ((sectors > 0) && (j < NUM_INDIRECT_BLOCK_PTRS)) {
      if (!free_map_allocate (1, &iib.ptr[j]))
        return false;
      block_write(fs_device, iib.ptr[j], zeros);
      sectors--;
      j++;
    }

    i++;

    while ((sectors > 0) && (i < NUM_INDIRECT_BLOCK_PTRS)) {
      size_t j = 0;
      if (!free_map_allocate (1, &ib.ptr[i]))
        return false;
      struct indirect_block iib;
      while ((sectors > 0) && (j < NUM_INDIRECT_BLOCK_PTRS)) {
        if (!free_map_allocate (1, &iib.ptr[j]))
          return false;
        block_write(fs_device, iib.ptr[j], zeros);
        sectors--;
        j++;
      }
      block_write(fs_device,ib.ptr[i],&iib);
      i++;
    }
  }

  return sectors;
}
